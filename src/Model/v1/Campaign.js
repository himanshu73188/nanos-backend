module.exports = class Campaign {

  // fetch campaign w.r.t id
  // -------------------------------
  // used by controllers/campaign.js - fetchCampaignById
  static fetchById(db, id) {
        
    return new Promise(function (resolve, reject) {
        db.query("SELECT * FROM campaigns WHERE id = ?", [id], function (err, row) {
            if (!err) {
                resolve(row[0]);
            } else {
                reject(err);
            }
        });
     });

  }

  // fetch all campaigns
  // -------------------------------
  // used by controllers/campaigns.js - fetchCampaigns
  static fetchAll(db) {
        
    return new Promise(function (resolve, reject) {
        db.query("SELECT * FROM campaigns", function (err, rows) {
            if (!err) {
                resolve(rows);
            } else {
                reject(err);
            }
        });
     });

  }

  // create a campaign
  // -------------------------------
  // used by controllers/campaign.js - addCampaign
  static add(db, campaignName, clientId, stripeChargeId) {
        
    return new Promise(function (resolve, reject) {
        db.query("INSERT INTO campaigns(name, client_id, stripe_charge_id) VALUES (?, ?, ?)", [campaignName, clientId, stripeChargeId], function (err, row) {
            if (!err) {
                resolve(row);
            } else {
                reject(err);
            }
        });
     });

  }

}