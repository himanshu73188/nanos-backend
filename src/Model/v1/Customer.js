module.exports = class Customer {

  // fetch all customers
  // -------------------------------
  // used by controllers/invoice.js - syncVAT
  static fetchAll(db) {
        
    return new Promise(function (resolve, reject) {
        db.query("SELECT * FROM customers", function (err, rows) {
            if (!err) {
                resolve(rows);
            } else {
                reject(err);
            }
        });
     });

  }

  // fetch customer w.r.t id
  // -------------------------------
  // used by controllers/customer.js - fetchCustomerById
  static fetchById(db, id) {
        
    return new Promise(function (resolve, reject) {
        db.query("SELECT * FROM customers WHERE id = ?", [id], function (err, row) {
            if (!err) {
                resolve(row[0]);
            } else {
                reject(err);
            }
        });
     });

  }

  // create a customer
  // -------------------------------
  // used by controllers/customer.js - addCustomer
  static add(db, customerName, country, vatNumber, stripeCustomerId) {
        
    return new Promise(function (resolve, reject) {
        db.query("INSERT INTO customers(name, country, vat_number, stripe_customer_id) VALUES (?, ?, ?, ?)", [customerName, country, vatNumber, stripeCustomerId], function (err, row) {
            if (!err) {
                resolve(row);
            } else {
                reject(err);
            }
        });
     });

  }

}