## Requirements.

---

* node v12.13.0
* npm 6.12.0
* Mysql
* Stripe account

## Configuration.

---

* `config/db_credentials.js` - This file contains configuration for the database.
* `config/stripe_credentials.js` - This file contains configuration for the stripe.

## Dependencies.

---

* [async](https://www.npmjs.com/package/async) - sync is a utility module which provides straight-forward, powerful functions for working with asynchronous JavaScript. Although originally designed for use with Node.js and installable via npm i async, it can also be used directly in the browser.
* [cors](https://www.npmjs.com/package/cors) - CORS is a node.js package for providing a Connect/Express middleware that can be used to enable CORS with various options.
* [express](https://www.npmjs.com/package/express) - Fast, unopinionated, minimalist web framework for node.
* [morgan](https://www.npmjs.com/package/morgan) - HTTP request logger middleware for node.js
* [mysql](https://www.npmjs.com/package/mysql) - This is a node.js driver for mysql. 
* [node-cron](https://www.npmjs.com/package/node-cron) - The node-cron module is tiny task scheduler in pure JavaScript for node.js based on GNU crontab. This module allows you to schedule task in node.js using full crontab syntax.
* [stripe](https://www.npmjs.com/package/stripe) - The Stripe Node library provides convenient access to the Stripe API from applications written in server-side JavaScript.
* [nodemon](https://www.npmjs.com/package/nodemon) - nodemon is a tool that helps develop node.js based applications by automatically restarting the node application when file changes in the directory are detected.

### How do I get set up? ###

---

Run commands in project terminal.

1. clone this repo via `git clone https://himanshu73188@bitbucket.org/himanshu73188/nanos-backend.git`.

2. Run `cd nanos-backend`.

3. Run `npm install`.

4. Run `npm start`, start App with Hot reloading using node_modules/nodemon/bin/nodemon.js app.js.

5. In postman, Check with `http://localhost:8080/:url`. Default Port:- 8080.

## Application Structure

---

* `app.js` - The entry point to our application. This file defines our express server and connects it to Mysql. It also requires the routes and stripe we'll be using in the application.
* `config/` - This folder contains configuration for mysql & stripe.
* `controllers/` - This folder contains the business logic of our app.
* `routes/` - This folder contains the route definitions for our API.
* `src/Model/` - This folder contains the models of our mysql database tables.

### What is this repository for? ###

* **Quick Summary**

    ---

    Nanos is based in Switzerland, and is hence required by laws to charge an additional VAT on the total price for any client from Switzerland - irrespective of what the client’s nationality is. Note that VAT should ONLY be charged to clients based in Switzerland. 
    The VAT rate is 7.7%. So for a client paying CHF100, we charge an additional CHF7.70 (as VAT). Clients usually request an “tax-invoice” with the net price (CHF100), tax (CHF7.70) and the total gross price (CHF107.70) - for tax and accounting purposes. Nanos is, by law, required to furnish this tax-invoice listing the following information: 

     * Nanos company contact details and address 
     * Clients company contact information and address 
     * Nanos Swiss VAT number (assume: CHE-123456-TEST) 
     * Clients VAT number (if supplied by customer, blank if not) 
     * Service provided by Nanos (generally the client’s ad campaign name and duration on Nanos.ai platform) 
     * Net price, VAT and Gross price 


* **The Task Summary**

    ---

    1. To insert the VAT numbers (if any) from Nanos internal tables into the Customer object inside Stripe. Stripe stores this as “tax_ids”. We are only concerned with Swiss VAT here. 

    2. Set the “tax_exempt” status in the Stripe Customer object to “none” for all Swiss customers and “exempt” for all non-Swiss customers. 

    3. Can you write a method that uses our Nanos APIs, the Stripe APIs and the new render_tax_invoice method to create a tax-invoice for a given ad campaign? The ad_campaign_id is an input to your method.

* **router Definitions**

    ---

    * `GET v1/customer/:id` - fetch customer w.r.t id
    * `POST v1/customer` - create a customer
    * `GET v1/campaigns` - fetch all campaigns
    * `GET v1/campaigns/campaign/:id` - fetch campaign w.r.t id
    * `POST v1/campaigns` - create a campaign
    * `GET v1/invoice/:campaignId` -  fetch invoice info
    * `GET v1/invoice/syncVAT` -  update vat numbers into stripe db

* **CRON jobs**

    ---

    update vat numbers into stripe db every 24 hours
```
    const cron                       = require('node-cron');

    // call syncVAT every 24 hours.
    cron.schedule('0 0 0 * * *', () => {
    syncVAT();
    });
```

## Steps to assign error code to newly created API call:

1. On Creating a new controller file, decide an unique error code in 1000s respectively.

2. If adding API call to already existed controller file, check already defined error code in starting of controller file and assign an incremental unique error code to newly created API call.

3. Already Used Error Codes are ( #1000, #2000, #3000 ).

### Who do I talk to? ###

* Repo admin - himanshu73188@gmail.com