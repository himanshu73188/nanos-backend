const express             = require("express");
const router              = express.Router();

// controllers
const customerController  = require('../../controllers/v1/customer');

// GET | /customers/customer/:id | fetch customer w.r.t id
// -------------------------------------------
router.get("/customer/:id", customerController.fetchCustomerById);

// POST | /customers | create a customer
// -------------------------------------------
router.post("/", customerController.addCustomer);

module.exports  =   router; 