const express             = require("express");
const router              = express.Router();

// controllers
const campaignController = require('../../controllers/v1/campaign');

// GET | /campaigns | fetch all campaigns
// -------------------------------------------
router.get("/", campaignController.fetchCampaigns);

// GET | /campaigns/campaign/:id | fetch campaign w.r.t id
// -------------------------------------------
router.get("/campaign/:id", campaignController.fetchCampaignById);

// POST | /campaigns | create a campaign
// -------------------------------------------
router.post("/", campaignController.addCampaign);

module.exports  =   router; 