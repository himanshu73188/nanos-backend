const express             = require("express");
const router              = express.Router();

// controllers
const invoiceController   = require('../../controllers/v1/invoice');

// GET | /invoice/:campaignId | fetch invoice info
// -------------------------------------------
router.get("/:campaignId", invoiceController.fetchInvoiceInfo);

// GET | /invoice/sync | update vat numbers into stripe db
// -------------------------------------------
router.get("/syncVAT", invoiceController.syncVAT);

module.exports  =   router; 