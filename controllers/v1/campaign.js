//Error code - 2000

// import modules
const async                          = require("async");

// models
const Customer                       = require("../../src/Model/v1/Customer"); 
const Campaign                       = require("../../src/Model/v1/Campaign"); 

// fetch campaign w.r.t id
exports.fetchCampaignById            = async function(req, res, next) {

  // db
  const db                           = req.con;

  // fetch campaign id from params
  const campaignId                   = req.params.id; 

  try {

    // fetch campaign w.r.t id
    const campaign                   = await Campaign.fetchById(db, campaignId);

    return res.status(200).send({"status": "success", "campaign" : campaign});

  }
  
  catch (err) {
      // return error
      return res.status(500).send({"status": "failure", "code": 2001, "error" : err});
  } 

};

// fetch campaigns
exports.fetchCampaigns               = async function(req, res, next) {

  // db
  const db                           = req.con;

  const campaignData                 = [];

  try {

    // fetch campaigns 
    const campaigns                  = await Campaign.fetchAll(db);

    // loop through the array - campaigns
    async.forEachOf( campaigns, async function( campaign, index ) {

      // fetch customer w.r.t id
      const customer               = await Customer.fetchById(db, campaign.client_id);

      // add final data to the object array - campaignData
      campaignData.push({
        'id'               : campaign.id,
        'name'             : campaign.name,
        'client_id'        : campaign.client_id,
        'stripe_charge_id' : campaign.stripe_charge_id,
        'customer_name'    : customer.name 
      });

    }, err => {
      if ( err ) {
         // return error
        return res.status(500).send({"status": "failure", "code": 2002, "error" : err});
      } else {
          // return success response
          return res.status(200).send({"status": "success", "data" : campaignData });
      }
    });

  }
  
  catch (err) {
      // return error
      return res.status(500).send({"status": "failure", "code": 2003, "error" : err});
  } 

};

// create a campaign
exports.addCampaign                  = async function(req, res, next) {

  // db
  const db                           = req.con;

  // get data from the request body
  const { campaignName, clientId, stripeChargeId }    = req.body;

  try {

    // create campaign
    const campaign                   = await Campaign.add(db, campaignName, clientId, stripeChargeId);

    return res.status(201).send({"status": "success", "campaignId" : campaign.insertId });

  }
  
  catch (err) {
      // return error
      return res.status(500).send({"status": "failure", "code": 2004, "error" : err});
  } 

};

