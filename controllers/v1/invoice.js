//Error code - 3000

// import modules
const async                      = require("async");
const cron                       = require('node-cron');

// models
const Customer                   = require("../../src/Model/v1/Customer"); 
const Campaign                   = require("../../src/Model/v1/Campaign"); 

// update vat numbers into stripe db
exports.syncVAT                  = async function(req, res, next) {

  // db
  const db                       = req.con;

  // stripe
  const stripe                   = req.stripe;

  try {
    
    // fetch all customers from the customer table
    const customers              = await Customer.fetchAll(db);
    
    // loop through the array - customers
    async.forEachOf( customers, async function( customer, index ) {

      // fetch stripe customer info
      const stripeCustomer       = await stripe.customers.retrieve(customer.stripe_customer_id);
      
      // get swiss code type
      swissCode                  = customer.vat_number ? customer.vat_number.substring(0,3).toLowerCase() : '';

      //  check if customer vat number belongs to swiss
      if(swissCode === 'che') {
        addtaxId                 = true;

        if(stripeCustomer.tax_ids.total_count > 0) {

          // if swiss tax id already exist, then do not add tax id
          let alreadyExist       = stripeCustomer.tax_ids.data.find(taxId => taxId.type === 'ch_vat');
          if(alreadyExist) {
            addtaxId             = false;
          }

        } 
        // add tax id
        if(addtaxId) {
          const taxId            = await stripe.customers.createTaxId(
            customer.stripe_customer_id,
            {type: 'ch_vat', value: customer.vat_number}
          );
        }

      }

      // update tax exempt of every customer
      let taxExempt             = "none";
      if(stripeCustomer.address && stripeCustomer.address.country === 'CH') {
        taxExempt               = "exempt";
      }

      const updatedtaxExempt    = await stripe.customers.update(
        customer.stripe_customer_id,
        {tax_exempt: taxExempt }
      );
      
    }, err => {
      if ( err ) {
          // return stripe error
          if(err.type) {
            return res.status(err.statusCode).send({"status": "failure", "code": 3001, "error": err.raw.message});
          }
          // return error 
          return res.status(500).send({"status": "failure", "code": 3002, "error": err});
      } else {
          // return success response
          return res.status(201).send({"status": "success"});
      }
    });

  }
  
  catch (err) {
      // return error
      return res.status(500).send({"status": "failure", "code": 3003, "error": err});
  } 

};

// call syncVAT every 24 hours.
cron.schedule('0 0 0 * * *', () => {
  syncVAT();
});

// fetch invoice info
exports.fetchInvoiceInfo         = async function(req, res, next) {

  // db
  const db                       = req.con;

  // stripe
  const stripe                   = req.stripe;

  // fetch campaign id from params
  const campaignId               = req.params.campaignId; 

  let invoiceInfo                = '';

  try {

    // fetch campaign w.r.t id
    const campaign               = await Campaign.fetchById(db, campaignId);

    // fetch customer w.r.t id
    const customer               = await Customer.fetchById(db, campaign.client_id);

    // fetch stripe customer info
    const stripeCustomer         = await stripe.customers.retrieve(customer.stripe_customer_id);

    // fetch charge info
    const charge                 = await stripe.charges.retrieve(campaign.stripe_charge_id);
    
    // check if charge customer belongs to nanos customer  
    if(charge.customer != customer.stripe_customer_id) {
      // return error
      return res.status(500).send({"status": "failure", "code": 3006, "error": "Stripe charge customer is different from Nanos customer"});
    }

    // get swiss code type
    swissCode                    = customer.vat_number ? customer.vat_number.substring(0,3).toLowerCase() : '';

    let vatAmount                = 0;
    let netAmount                = charge.amount;

    // if tax code exists, update vatAmount & netAmount
    if(swissCode === 'che') {
      vatAmount                 = (7.7/100)*charge.amount;
      netAmount                 = charge.amount - vatAmount;
    }
    
    invoiceInfo                  = {
      'clientName'               : stripeCustomer.name,
      'email'                    : stripeCustomer.email,
      'address'                  : stripeCustomer.address,
      'clientVat'                : customer.vat_number,
      'campaignName'             : campaign.name,
      'invoiceCurrency'          : charge.currency,
      'invoiceAmount'            : charge.amount,
      'vatAmount'                : vatAmount,
      'netAmount'                : netAmount
    }; 

    // return success response
    return res.status(200).send({"status": "success", "invoiceInfo" : invoiceInfo});
  }

  catch (err) {
    // return stripe error
    if(err.type) {
      return res.status(err.statusCode).send({"status": "failure", "code": 3004, "error": err.raw.message});
    }
    // return error
    return res.status(500).send({"status": "failure", "code": 3005, "error": err});
  }

};