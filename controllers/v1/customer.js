//Error code - 1000

// import modules
// ---

// models
const Customer                       = require("../../src/Model/v1/Customer"); 

// fetch customer w.r.t id
exports.fetchCustomerById            = async function(req, res, next) {

  // db
  const db                           = req.con;

  // fetch customer id from params
  const customerId                   = req.params.id; 

  try {

    // fetch customer w.r.t id
    const customer                   = await Customer.fetchById(db, customerId);

    return res.status(200).send({"status": "success", "customer" : customer});

  }
  
  catch (err) {
      // return error
      return res.status(500).send({"status": "failure", "code": 1001, "error" : err});
  } 

};

// create a customer
exports.addCustomer                  = async function(req, res, next) {

  // db
  const db                           = req.con;

  // get data from the request body
  const { customerName, country, vatNumber, stripeCustomerId }    = req.body;

  try {

    // create customer
    const customer                   = await Customer.add(db, customerName, country, vatNumber, stripeCustomerId);

    return res.status(201).send({"status": "success", "customerId" : customer.insertId });

  }
  
  catch (err) {
      // return error
      return res.status(500).send({"status": "failure", "code": 1002, "error" : err});
  } 

};